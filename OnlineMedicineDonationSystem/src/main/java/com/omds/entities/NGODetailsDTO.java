package com.omds.entities;

import java.util.List;

public class NGODetailsDTO {
	
	private List<NGODetails> ngodetails;

	public List<NGODetails> getNgodetails() {
		return ngodetails;
	}

	public void setNgodetails(List<NGODetails> ngodetails) {
		this.ngodetails = ngodetails;
	}
	

}

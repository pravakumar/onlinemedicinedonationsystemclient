package com.omds.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Executive {
	
	private int executiveid;
	@Pattern(regexp="^[a-zA-Z0-9]{6,20}$",message="Username should contain AlphaNumeric Characters and should Contain 6-20 characters")
	private String username;
	@Pattern(regexp = "^[(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$)]{8,20}$",message="Password Should Contain atleast one Lower Case,Upper Case,Numbers,Special Characters and Should Conatin 8-20 characters")
	private String password;
	private String emailid;
	@NotBlank
	private String address;
	@Size(min=8,max=15,message="Contact Number should include 9-15 Digits")
	private String contact_num;
	public int getExecutiveid() {
		return executiveid;
	}
	public void setExecutiveid(int executiveid) {
		this.executiveid = executiveid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_num() {
		return contact_num;
	}
	public void setContact_num(String contact_num) {
		this.contact_num = contact_num;
	}
	
	
	
	public Executive() {
		super();
	}
	public Executive(int executiveid, String username, String password, String emailid, String address,
			String contact_num) {
		super();
		this.executiveid = executiveid;
		this.username = username;
		this.password = password;
		this.emailid = emailid;
		this.address = address;
		this.contact_num = contact_num;
	}
	@Override
	public String toString() {
		return "Executive [ExecutiveId=" + executiveid + ", username=" + username + ", password=" + password
				+ ", emailid=" + emailid + ", address=" + address + ", contact_num=" + contact_num + "]";
	}
	

}

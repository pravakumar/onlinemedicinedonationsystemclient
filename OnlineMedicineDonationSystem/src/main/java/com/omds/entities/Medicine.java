package com.omds.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class Medicine {
	
//	private int medicineid;
	@NotBlank
	@Pattern(regexp="^[a-zA-Z0-9]{3,20}$",message="Medicine name should only contain AlphaNumeric Characters and should Contain 3-20 characters")
	private String medicine_name;
	@NotBlank
//	@Pattern(regexp = "^[0-9]{2,4}$",m)
	private float medicine_price;
	@NotBlank
	private int quantity;
	@NotBlank
	@Pattern(regexp="^[0-9]{4}-0?[1-9]|1[012]-0?[1-9]|[12][0-9]|3[01]$",message="Manufacturing date should be in YYYY-MM-DD")
	private String mfg_date;
	@NotBlank
	@Pattern(regexp="^[0-9]{4}-0?[1-9]|1[012]-0?[1-9]|[12][0-9]|3[01]$",message="Expire date should be in YYYY-MM-DD")
	private String expire_date;
	public String getMedicine_name() {
		return medicine_name;
	}
	public void setMedicine_name(String medicine_name) {
		this.medicine_name = medicine_name;
	}
	public float getMedicine_price() {
		return medicine_price;
	}
	public void setMedicine_price(float medicine_price) {
		this.medicine_price = medicine_price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getMfg_date() {
		return mfg_date;
	}
	public void setMfg_date(String mfg_date) {
		this.mfg_date = mfg_date;
	}
	public String getExpire_date() {
		return expire_date;
	}
	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}
	@Override
	public String toString() {
		return "Medicine [medicine_name=" + medicine_name + ", medicine_price=" + medicine_price + ", quantity="
				+ quantity + ", mfg_date=" + mfg_date + ", expire_date=" + expire_date + "]";
	}
	public Medicine(String medicine_name, float medicine_price, int quantity, String mfg_date, String expire_date) {
		super();
		this.medicine_name = medicine_name;
		this.medicine_price = medicine_price;
		this.quantity = quantity;
		this.mfg_date = mfg_date;
		this.expire_date = expire_date;
	}
	public Medicine() {
		super();
	}
	
	

}

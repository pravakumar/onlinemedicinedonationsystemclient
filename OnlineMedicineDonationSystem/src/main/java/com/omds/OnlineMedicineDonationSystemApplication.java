package com.omds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMedicineDonationSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineMedicineDonationSystemApplication.class, args);
	}

}

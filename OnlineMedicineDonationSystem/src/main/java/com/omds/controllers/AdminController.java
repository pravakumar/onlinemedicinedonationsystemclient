package com.omds.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.omds.AdminLogin;
import com.omds.entities.Executive;
import com.omds.entities.Medicine;
import com.omds.entities.MedicineDTO;
import com.omds.entities.NGODetails;
import com.omds.entities.NGODetailsDTO;
import com.omds.entities.UserDTO;

@Controller
public class AdminController {
	
	@Autowired
	private RestTemplate resttemplate;
	
	//for getting adminlogin
		@GetMapping(value="/admin")
		public String admin() {
			return "adminlogin";
		}
		
		//for getting adminHomepage
		@GetMapping(value="adminHomePage")
		public String adminHomePage() {
			return "adminloggedin";
		}
		
		//for posting adminCrediantials and Authenticating
		@PostMapping(value="/adminlogin")
		public String adminlogin(AdminLogin adminlogin,Model model) {
			System.out.println(adminlogin);
			try {
//				resttemplate.getForObject("http://localhost:8086/adminlogin\"",User.class)r
				resttemplate.postForObject("http://localhost:8086/adminlogin", adminlogin, AdminLogin.class);
//				model.addAttribute("ngodetails", new NGODetails());
				return "adminloggedin";
			}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("message","Username or Password is Incorrect");
				return "adminlogin";
			}
			
		}
		//for getting the addngo.jsp
		@GetMapping(value="/addngo")
		public String Ngo(Model model) {
			NGODetails ngodetails=new NGODetails();
			model.addAttribute("ngodetails", ngodetails);
			return "addngo";
		}
		
		//for posting the ngodetails
		@PostMapping(value="/ngoDetailsSave")
		public String NgoSave(@Valid @ModelAttribute("ngodetails") NGODetails ngodetails,BindingResult br) {
			System.out.println(ngodetails);
			if(br.hasErrors()) {
				return "addngo";
			}
			resttemplate.postForObject("http://localhost:8086/ngodetails", ngodetails, String.class);
			return "adminloggedin";
		}
		
		
		//for getting the medicine
		@GetMapping(value="/addmedicine")
		public String AddMedicine(Model model) {
			Medicine medicine=new Medicine();
			model.addAttribute("medicine", medicine);
			return "addmedicine";
		}
		//for posting medicine details 
		@PostMapping(value="/savemedicine")
		public String SaveMedicine(@ModelAttribute("medicine")Medicine medicine,BindingResult br) {
			
			if(br.hasErrors()) {
				return "addmedicine";
			}
			resttemplate.postForObject("http://localhost:8086/addmedicine", medicine, String.class);
			return "adminloggedin";
		}
		//for viewing ngo details
		@GetMapping(value="/viewngo")
		public String ViewNgo(Model model) {
			 NGODetailsDTO ngodetails=resttemplate.getForObject("http://localhost:8086/viewNGO", NGODetailsDTO.class);
			
//			 dto.setNgodetails(null);
			System.out.println(ngodetails);
			model.addAttribute("ngodetails", ngodetails.getNgodetails());
			return "viewngo";
		}
		//for getting all the medicine
		@GetMapping(value="/viewmedicine")
		public String ViewMedicine(Model model) {
			 MedicineDTO medicine=resttemplate.getForObject("http://localhost:8086/viewMedicine", MedicineDTO.class);
			
//			 dto.setNgodetails(null);
//			System.out.println(ngodeta);
			model.addAttribute("medicinedetails", medicine.getMedicineList());
			return "viewmedicine";
		}
		//for getting all the user
		@GetMapping(value="/viewuser")
		public String ViewUsers(Model model) {
			 UserDTO userdetails=resttemplate.getForObject("http://localhost:8086/viewUsers", UserDTO.class);
			
//			 dto.setNgodetails(null);
//			System.out.println(ngodetails);
			model.addAttribute("userdetails", userdetails.getUserList());
			return "viewuser";
		}
		
		//for getting ngoLogin Page
		@GetMapping(value="/ngo")
		public String NgoLogin() {
//			NGODetails ngodetails=new NGODetails();
//			model.addAttribute("ngodetails", ngodetails);
			return "ngologin";
		}
		
		//for validating ngologinId and password
		@PostMapping(value="/ngologin")
		public String NgoLoginValidation(NGODetails ngodetails,Model model,HttpSession session) {
			System.out.println(ngodetails);
			try {
				NGODetails ngo=resttemplate.postForObject("http://localhost:8086/validatengo", ngodetails, NGODetails.class);
				System.out.println(ngo);
				session.setAttribute("ngoid", ngo.getId());
				if(ngo.getFlag()==1) {
					return "ngoresetpassword";
				}
				session.setAttribute("ngoname", ngo.getName());
				return "ngohomepage";
			}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("message", "Username and Password Not Matching");
				return "ngologin";
				
			}
			
		}
		
		//for validating current and new password of ngo
		@PostMapping(value="/ngoreset")
		public String NgoResetPassword(NGODetails ngodetails,@RequestParam("newpassword")String newpassword,@RequestParam("repeatpassword")
		String repeatpassword,HttpSession session,Model model) {
//			int ngoID=session.getAttribute(repeatpassword)
			int ngoId=(int) session.getAttribute("ngoid");
			NGODetails ngo=resttemplate.getForObject("http://localhost:8086/validatengo/"+ngoId,NGODetails.class);
			
				if(ngo.getPassword().equals(ngodetails.getPassword()) && (newpassword.equals(repeatpassword))) {
					int flag=2;
					NGODetails ngonewdetails=new NGODetails();
					ngonewdetails.setId(ngo.getId());
					ngonewdetails.setName(ngo.getName());
					ngonewdetails.setEmailid(ngo.getEmailid());
					ngonewdetails.setPassword(repeatpassword);
					ngonewdetails.setContact_num(ngo.getContact_num());
					ngonewdetails.setCity(ngo.getCity());
					ngonewdetails.setState(ngo.getState());
					ngonewdetails.setFlag(flag);
					resttemplate.postForObject("http://localhost:8086/ngodetailsUpdate", ngonewdetails, String.class);
				session.setAttribute("ngoname",ngo.getName() );
				return "ngohomepage";
				}
				else {
				model.addAttribute("message","Check the Current Password and New Password" );
				return "ngoresetpassword";
				}
			
//			return "ngoresetpassword";
			
			
		}
		
		
		//for getting executive login page
		@GetMapping(value="/executive")
		public String executiveLogin() {
			return "executivelogin";
		}
		
		//for getting executive register page
		@GetMapping(value="/executiveregister")
		public String ExecutiveRegister(Model model) {
			Executive executive=new Executive();
			model.addAttribute("executive", executive);
			return "executiveregister";
		}
		
		//for posting executive register credientials
		@PostMapping(value="/executiveRegister")
		public String ExecutiveRegsiter(@Valid @ModelAttribute("executive") Executive executive,BindingResult br) {
			System.out.println(executive);
			if(br.hasErrors()) {
				
				return "executiveregister";
			}else {
				
				resttemplate.postForObject("http://localhost:8086/executiveregister", executive,String.class);
				return "executivelogin";
			}
			
		}
		
		//for authenticating executive login credientials
		@PostMapping(value="/executivelogin")
		public String AuthenticateExecutive(Executive executive,Model model) {
			try {
				resttemplate.postForObject("http://localhost:8086/executivelogin", executive,Executive.class);
				return "executivehomepage";
			}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("message", "Username or Password Invalid");
				return "executivelogin";
			}
		}
		
		
		//end of admin controller
		//testing dev branch

}

package com.omds.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.omds.AdminLogin;
import com.omds.entities.MedicineDTO;
import com.omds.entities.User;


@Controller
public class UserController {
	@Autowired
	private RestTemplate resttemplate;
	
	//for getting index page
	@GetMapping(value="/index")
	public String index() {
		return "index";
	}
	
	
	
	//for user login
	@GetMapping(value="/user")
	public String userlogin() {
		return "userLogin";
	}
	//for posting username and password for user validation
	@PostMapping(value="/userlogin")
	public String userLogin(User user,Model model,HttpSession session) {
		try {
			User userObj=resttemplate.postForObject("http://localhost:8086/userlogin", user,User.class);
			session.setAttribute("username", userObj.getUsername());
			return "userHomePage";
		}catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("message", "Username or Password is Incorrect");
			return "userLogin";
		}
		
	}
	//for getting all the medicines from DB
	@GetMapping(value="/buymedicine")
	public String MedicineList(Model model) {
		MedicineDTO medicine=resttemplate.getForObject("http://localhost:8086/viewMedicine", MedicineDTO.class);
		model.addAttribute("medicinedetails", medicine.getMedicineList());
		return "buymedicine";
	}
	
	
	
	
	
	
	
	
	//for user registration
	@GetMapping(value="/register")
	public String registerUser(Model model) {
		User user=new User();
		model.addAttribute("user", user);
	
		return "userRegister";
	}
	
	//after registrating user login
	@PostMapping(value="/userRegister")
	public String userLogin(@Valid @ModelAttribute("user") User user,BindingResult br) {
		System.out.println(user);
		
		if(br.hasErrors()) {
			return "userRegister";
		}
		resttemplate.postForObject("http://localhost:8086/register", user, String.class);
		return "userLogin";
	}
	
	
	

}

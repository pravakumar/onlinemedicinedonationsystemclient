<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">
body {
	background: #007bff;
	background: linear-gradient(to right, #0062E6, #33AEFF);
}

.card-signin {
	border: 0;
	border-radius: 1rem;
	box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
	margin-bottom: 2rem;
	font-weight: 300;
	font-size: 1.5rem;
}

.card-signin .card-body {
	padding: 2rem;
}
}
</style>
<script>
function myFunction() {
      
	  var passwd = '';
	  var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	  for (i=0;i<8;i++) {
	    var c = Math.floor(Math.random()*chars.length + 1);
	    passwd += chars.charAt(c)
	  }
	  var userid=Math.floor(Math.random() * 100)
	  alert("Successfully Added NGO with ID:"+userid);
	  var flag=1;
	  alert(flag);
	  document.getElementById("userid").value = userid;
	  
	  document.getElementById("passwd").value = passwd;
	  document.getElementById("flag").value = flag;
	}
</script>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">NGO Regiser</h5>

						<form:form action="ngoDetailsSave" method="post" modelAttribute="ngodetails" onsubmit="myFunction()">
							<div class="form-group">

								<label for="name">NGO Name</label> <input type="text"
									class="form-control" name="name" id="name" required>
									<form:errors path="name" cssStyle="color:red"></form:errors>
							</div>
	
							<div class="form-group">
								<label for="emailid">Email-Id</label> <input type="email"
									class="form-control" name="emailid" id="emailid" required>
							</div>
							<div class="form-group">
								<label for="address">City</label> <input type="text"
									class="form-control" name="city" id="city" required>
									<form:errors path="city" cssStyle="color:red"></form:errors>
							</div>
							<div class="form-group">
								<label for="address">State</label> <input type="text"
									class="form-control" name="state" id="state" required>
									<form:errors path="state" cssStyle="color:red"></form:errors>
							</div>
							<div class="form-group">
								<label for="contact_num">Contact Number</label> <input
									type="text" class="form-control" name="contact_num"
									id="contact_num" required>
									<form:errors path="contact_num" cssStyle="color:red"></form:errors>
							</div>
							<form:hidden path="id" id="userid"/>
							
							<form:hidden path="password" id="passwd"/>
							<form:hidden path="flag" id="flag"/>

							<input type="submit" class="btn btn-success" value="Register">

						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
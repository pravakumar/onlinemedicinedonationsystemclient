<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style type="text/css">
body {
	background: #007bff;
	background: linear-gradient(to right, #0062E6, #33AEFF);
}

.card-signin {
	border: 0;
	border-radius: 1rem;
	box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
	margin-bottom: 2rem;
	font-weight: 300;
	font-size: 1.5rem;
}

.card-signin .card-body {
	padding: 2rem;
}
#mess{
color:red;
}

</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		var num = "${check}";
		var mes = "${message}";
		console.log(num);

		if (num == 1) {
			$("#username").css({
				"border" : "2px solid red"
			});
			$("#password").css({
				"border" : "2px solid red"
			});
			$("#mess").text(mes).css({
				"color" : "red",
				"font-weight" : "Helvetica"
			});
		}

		$("#username").click(function() {
			$(this).css({
				"border" : ""
			});
			$("#password").css({
				"border" : ""
			});
			$("#mess").text("");
			num = 0;
		})

	});
</script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index">Online Medicine Donation System</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="admin">Admin <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="executive">Executive</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="user">User</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ngo">NGO</a>
      </li>
    </ul>
  </div>
</nav>




	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">User Sign In</h5>

						<form:form action="userlogin" method="post"
							modelAttribute="user">
							<div class="form-group">

								<label for="username">Enter Username</label> <input
									type="text" class="form-control" name="username" id="username"
									placeholder="UserName" required>
							</div>
							<div class="form-group">
								<label for="password">Enter Password</label> <input
									type="password" class="form-control" name="password"
									id="password" placeholder="Password" required>
							</div>
							<div id="mess">${message }</div>

							<br>

							<input type="submit" class="btn btn-success" value="Login">
							
							<a href="register">If New User,Register Here</a>

						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
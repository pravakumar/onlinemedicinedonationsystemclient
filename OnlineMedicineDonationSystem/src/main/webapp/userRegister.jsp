<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">
body {
	background: #007bff;
	background: linear-gradient(to right, #0062E6, #33AEFF);
}

.card-signin {
	border: 0;
	border-radius: 1rem;
	box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
	margin-bottom: 2rem;
	font-weight: 300;
	font-size: 1.5rem;
}

.card-signin .card-body {
	padding: 2rem;
}
}
</style>
<script>
function myFunction() {
	  alert("SuccessFully Registered");
	}
</script>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">User Regiser</h5>

						<form:form action="userRegister" method="post" modelAttribute="user" onsubmit="myFunction()">
							<div class="form-group">

								<label for="username">Enter Username</label> <input type="text"
									class="form-control" name="username" id="username" required>
									<form:errors path="username" cssStyle="color:red"></form:errors>
							</div>
							<div class="form-group">
								<label for="password">Enter Password</label> <input
									type="password" class="form-control" name="password"
									id="password" required>
									<form:errors path="password" cssStyle="color:red"></form:errors>
							</div>
							<div class="form-group">
								<label for="emailid">Enter EmailId</label> <input type="email"
									class="form-control" name="emailid" id="emailid" required>
							</div>
							<div class="form-group">
								<label for="address">Enter Address</label> <input type="text"
									class="form-control" name="address" id="address" required>
							</div>
							<div class="form-group">
								<label for="contact_num">Enter Contact Number</label> <input
									type="text" class="form-control" name="contact_num"
									id="contact_num" required>
									<form:errors path="contact_num" cssStyle="color:red"></form:errors>
							</div>
							<div id="mess"></div>

							<div id="mess"></div>

							<input type="submit" class="btn btn-success"  value="Register">

						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
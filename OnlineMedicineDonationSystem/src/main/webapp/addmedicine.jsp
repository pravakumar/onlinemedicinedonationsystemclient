<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">
body {
	background: #007bff;
	background: linear-gradient(to right, #0062E6, #33AEFF);
}

.card-signin {
	border: 0;
	border-radius: 1rem;
	box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
	margin-bottom: 2rem;
	font-weight: 300;
	font-size: 1.5rem;
}

.card-signin .card-body {
	padding: 2rem;
}
}
</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Add Medicine</h5>

						<form:form action="savemedicine" method="post" modelAttribute="medicine">
							<div class="form-group">

								<label for="medicine_name">Medicine Name</label> <input type="text"
									class="form-control" name="medicine_name" id="medicine_name" placeholder="Name" required>
									
							</div>
	
							<div class="form-group">
								<label for="medicine_price">Medicine Price</label> <input type="text"
									class="form-control" name="medicine_price" id="medicine_price" placeholder="Price" required>
							</div>
							<div class="form-group">
								<label for="quantity">Quantity</label> <input type="text"
									class="form-control" name="quantity" id="quantity" placeholder="Quantity" required>
									
							</div>
							<div class="form-group">
								<label for="mfg_date">Manufacturing Date</label> <input type="text"
									class="form-control" name="mfg_date" id="mfg_date" placeholder="YYYY-MM-DD" required>
									
							</div>
							<div class="form-group">
								<label for="expire_date">Expire Date</label> <input type="text"
									class="form-control" name="expire_date" id="expire_date" placeholder="YYYY-MM-DD" required>
									
							</div>
							<div id="mess"></div>

							<div id="mess"></div>

							<input type="submit" class="btn btn-success" value="Add">

						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
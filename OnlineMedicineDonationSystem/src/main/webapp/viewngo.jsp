<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Online Medicine Donation System</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="adminHomePage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewuser">View Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewmedicine">View Medicine</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="admin">Logout</a>
      </li>
    </ul>
  </div>
</nav>
<h3>NGO Details</h3>
<table border ="3" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>NGO ID</th>
							<th>NGO Name</th>
							<th>Email ID</th>
							<th>City</th>
							<th>State</th>
							<th>Phone Number</th>
							
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${ngodetails}" var="ngo">
						<tr>
							<td>${ngo.id }</td>
							<td>${ngo.name}</td>
							<td>${ngo.emailid }</td>
							<td>${ngo.city }</td>
							<td>${ngo.state }</td>
							
							<td>${ngo.contact_num }</td>
							
								
						</tr>
						
						
											
						</c:forEach>
					</tbody>
				</table>
</body>
</html>
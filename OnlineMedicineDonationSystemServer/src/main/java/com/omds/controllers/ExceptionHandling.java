package com.omds.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

//import com.library.exceptions.ErrorDetails;
//import com.library.exceptions.ResourceNotFoundException;
@ControllerAdvice
public class ExceptionHandling {
	
	@ExceptionHandler(com.omds.exceptions.ResourceNotFoundException.class)
	public ResponseEntity<Object> handleResouseNotFoundException(com.omds.exceptions.ResourceNotFoundException
			ex) {

		com.omds.exceptions.ErrorDetails errors = new com.omds.exceptions.ErrorDetails(ex.getMessage());

		return new ResponseEntity<Object>(errors, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(com.omds.exceptions.InvalidUsernameAndPassword.class)
	public ResponseEntity<Object> handleResouseNotFoundException(com.omds.exceptions.InvalidUsernameAndPassword
			ex) {

		com.omds.exceptions.ErrorDetails errors = new com.omds.exceptions.ErrorDetails(ex.getMessage());

		return new ResponseEntity<Object>(errors, HttpStatus.NOT_FOUND);
	}

}

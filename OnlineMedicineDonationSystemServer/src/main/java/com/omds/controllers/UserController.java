package com.omds.controllers;

import javax.jws.soap.SOAPBinding.Use;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.omds.entities.AdminLogin;
import com.omds.entities.User;
//import com.omds.services.AdminLoginServices;
import com.omds.services.UserServices;

@RestController
//@RequestMapping(value="api/")
public class UserController {
	
	@Autowired
	private UserServices userservice;
	
	//for posting user login credientials
	@PostMapping(value="/userlogin")
	public ResponseEntity<?> getvalidateUserLogin(@RequestBody User user)
	{
		System.out.println(user);
		User userObject=userservice.findUserByName(user.getUsername());
		User userObj=userservice.ValidateUser(user.getUsername(),user.getPassword());
		
		return new ResponseEntity<User>(userObj,HttpStatus.OK);
		
	}
	
	//for posting user registration credientials
	@PostMapping(value="/register")
	public ResponseEntity<?> UserRegister(@RequestBody User user) {
		userservice.registerUser(user);
		return new ResponseEntity<String>("Registration Successful",HttpStatus.OK);
	}
}

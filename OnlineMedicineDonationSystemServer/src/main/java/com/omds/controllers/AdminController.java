package com.omds.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.omds.entities.AdminLogin;
import com.omds.entities.Executive;
import com.omds.entities.Medicine;
import com.omds.entities.MedicineDTO;
import com.omds.entities.NGODetails;
import com.omds.entities.NGODetailsDTO;
import com.omds.entities.User;
import com.omds.entities.UserDTO;
import com.omds.services.AdminServices;
//import com.omds.services.AdminLoginServices;

@RestController
public class AdminController {
	
	@Autowired
	private AdminServices adminservice;
	//for posting admin credientials and validating
	@PostMapping("/adminlogin")
	public ResponseEntity<?> getvalidate(@RequestBody AdminLogin adminlogin)
	{
		System.out.println(adminlogin);
		AdminLogin admin=adminservice.adminAuthenticate(adminlogin.getUsername(), adminlogin.getPassword());
		return new ResponseEntity<AdminLogin>(admin,HttpStatus.OK);
		
	}
	
	//for posting ngodetails to DB
	@PostMapping(value="/ngodetails")
	public ResponseEntity<?> NgoDetails(@RequestBody NGODetails ngodetails)
	{
		boolean res=adminservice.NgoDetails(ngodetails);
		System.out.println(res);
		return new ResponseEntity<String>("NGODetails Added Successfully",HttpStatus.OK);
	}
	
	//for getting all the users
	@GetMapping(value="/viewUsers")
	public ResponseEntity<UserDTO> viewUsers(){
		UserDTO userdto=new UserDTO();
		
		userdto.setUserList(adminservice.getAllUsers());
		return new ResponseEntity<UserDTO>(userdto,HttpStatus.OK);
	}
	
	//for getting all the medicines
	
		@GetMapping(value="/viewMedicine")
		public ResponseEntity<MedicineDTO> viewMedicine(){
			MedicineDTO medicinedto=new MedicineDTO();
			
			medicinedto.setMedicineList(adminservice.getAllMedicine());
			return new ResponseEntity<MedicineDTO>(medicinedto,HttpStatus.OK);
		}
	
	//for getting all the NGO
	@GetMapping(value="/viewNGO")
	public ResponseEntity<?> viewNGO(){
		NGODetailsDTO dto=new NGODetailsDTO();
		dto.setNgodetails(adminservice.getAllNGO());
		return new ResponseEntity<NGODetailsDTO>(dto,HttpStatus.OK);
	}
	
	//for posting Medicine
	@PostMapping(value="/addmedicine")
	public ResponseEntity<?> addMedicine(@RequestBody Medicine medicine){
		adminservice.AddMedicine(medicine);
		return new ResponseEntity<String>("Medicine Added Successfully",HttpStatus.OK);
		
	}
	//for checking and validating ngodetails
	@PostMapping(value="/validatengo")
	public ResponseEntity<NGODetails> AuthenticateNgo(@RequestBody NGODetails ngodetails){
		adminservice.findNgoById(ngodetails.getId());
		NGODetails ngo=adminservice.ValidateNgo(ngodetails.getId(), ngodetails.getPassword());
		return new ResponseEntity<NGODetails>(ngo,HttpStatus.OK);
		
	}
	//for checking and validating new password of ngo
	@GetMapping(value="/validatengo/{id}")
	public ResponseEntity<NGODetails> AuthenticateNgoById(@PathVariable("id") int id){
		NGODetails ngo=adminservice.findNgoById(id);
		
		return new ResponseEntity<NGODetails>(ngo,HttpStatus.OK);
		
	}
	
	//for updating ngodetails
	@PostMapping(value="/ngodetailsUpdate")
	public ResponseEntity<?> UpdateNgoDetails(@RequestBody NGODetails ngodetails){
		adminservice.UpdateNgoDetais(ngodetails.getId(), ngodetails.getFlag(), ngodetails.getPassword());
		return new ResponseEntity<String>("Updated NgoDetails Successfully",HttpStatus.OK);
	}
	
	//for saving executive details into DB
	@PostMapping(value="/executiveregister")
	public ResponseEntity<?> AddExecutive(@RequestBody Executive executive){
		adminservice.AddExecutive(executive);
		return new ResponseEntity<String>("Executive Details Added Succcessfully",HttpStatus.OK);
	}
	
	//for authenticating executive credientials
	@PostMapping(value="/executivelogin")
	public ResponseEntity<?> AuthenticateExecutive(@RequestBody Executive executive){
		adminservice.findByExecutiveId(executive.getUsername());
		Executive exe=adminservice.AuthenticateExecutive(executive.getUsername(), executive.getPassword());
		return new ResponseEntity<Executive>(exe,HttpStatus.OK);
	}

}

package com.omds.entities;

import java.util.List;

public class MedicineDTO {
	
	private List<Medicine> medicineList;

	public List<Medicine> getMedicineList() {
		return medicineList;
	}

	public void setMedicineList(List<Medicine> medicineList) {
		this.medicineList = medicineList;
	}
	
}

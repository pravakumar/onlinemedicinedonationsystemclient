package com.omds.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class Medicine {
	
	private int medicineid;
	
	private String medicine_name;
	
	private float medicine_price;
	
	private int quantity;
	
	private String mfg_date;
	private String expire_date;
	public int getMedicineid() {
		return medicineid;
	}
	public void setMedicineid(int medicineid) {
		this.medicineid = medicineid;
	}
	public String getMedicine_name() {
		return medicine_name;
	}
	public void setMedicine_name(String medicine_name) {
		this.medicine_name = medicine_name;
	}
	public float getMedicine_price() {
		return medicine_price;
	}
	public void setMedicine_price(float medicine_price) {
		this.medicine_price = medicine_price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getMfg_date() {
		return mfg_date;
	}
	public void setMfg_date(String mfg_date) {
		this.mfg_date = mfg_date;
	}
	public String getExpire_date() {
		return expire_date;
	}
	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}
	@Override
	public String toString() {
		return "Medicine [medicineid=" + medicineid + ", medicine_name=" + medicine_name + ", medicine_price="
				+ medicine_price + ", quantity=" + quantity + ", mfg_date=" + mfg_date + ", expire_date=" + expire_date
				+ "]";
	}
	public Medicine(int medicineid, String medicine_name, float medicine_price, int quantity, String mfg_date,
			String expire_date) {
		super();
		this.medicineid = medicineid;
		this.medicine_name = medicine_name;
		this.medicine_price = medicine_price;
		this.quantity = quantity;
		this.mfg_date = mfg_date;
		this.expire_date = expire_date;
	}
	public Medicine() {
		super();
	}
	
	
	
	

}

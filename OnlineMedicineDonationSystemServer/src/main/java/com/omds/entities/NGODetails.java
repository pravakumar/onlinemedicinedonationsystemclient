package com.omds.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NGODetails {
	
	private int id;
	private String name;
	private String emailid;
	private String password;
	private String city;
	private String state;
	private String contact_num;
	private int flag;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getContact_num() {
		return contact_num;
	}
	public void setContact_num(String contact_num) {
		this.contact_num = contact_num;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	@Override
	public String toString() {
		return "NGODetails [id=" + id + ", name=" + name + ", emailid=" + emailid + ", password=" + password + ", city="
				+ city + ", state=" + state + ", contact_num=" + contact_num + ", flag=" + flag + "]";
	}
	public NGODetails() {
		super();
	}
	public NGODetails(int id, String name, String emailid, String password, String city, String state,
			String contact_num, int flag) {
		super();
		this.id = id;
		this.name = name;
		this.emailid = emailid;
		this.password = password;
		this.city = city;
		this.state = state;
		this.contact_num = contact_num;
		this.flag = flag;
	}
	



	
	
	

	
	
	
	
}

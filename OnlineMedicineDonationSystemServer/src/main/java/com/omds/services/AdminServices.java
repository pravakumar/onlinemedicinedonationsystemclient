package com.omds.services;

import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omds.dao.AdminDao;
//import com.omds.dao.AdminLoginDao;
import com.omds.entities.AdminLogin;
import com.omds.entities.Executive;
import com.omds.entities.Medicine;
import com.omds.entities.NGODetails;
import com.omds.entities.User;
import com.omds.exceptions.InvalidUsernameAndPassword;
import com.omds.exceptions.ResourceNotFoundException;

//import com.OnlineMedicineDonation.Dao.AdminLoginDao;
//import com.OnlineMedicineDonation.Model.AdminLogin;

@Service
public class AdminServices {

	@Autowired
	AdminDao dao;
	//for validating admin credientials
	public AdminLogin adminAuthenticate(String username,String password) {
		AdminLogin admin=dao.adminAuthenticate(username);
		System.out.println(admin);
		if(!(admin.getPassword().equals(password))) {
			throw new InvalidUsernameAndPassword("Username and Password Not Matching");
		}
		else {
			return admin;
		}
		
	}
	//for adding ngodetails into DB
	public boolean NgoDetails(NGODetails ngodetails) {
		return dao.AddNgo(ngodetails);
	}
	
	//for viewing all the users
	public List<User> getAllUsers(){
		return dao.ViewUsers();
	}
	
	//for viewing all the medicine
	public List<Medicine> getAllMedicine(){
		return dao.ViewMedicine();
	}
	//for viewing all the ngo
	public List<NGODetails> getAllNGO(){
		return dao.ViewNgo();
	}
	//for adding medicine into DB
	public boolean AddMedicine(Medicine medicine) {
		return dao.AddMedicine(medicine);
	}
	//for viewing all the ngo
	public List<NGODetails> ViewNgo(){
		return dao.ViewNgo();
	}
	//for finding ngodetails by id for validation
	public NGODetails findNgoById(int id) {
		NGODetails ngodetails=dao.findById(id);
		if(ngodetails==null) {
			throw new ResourceNotFoundException("No NGO with NgoId with"+id);
		}
		return ngodetails;
	}
	//for validation ngoid and password
	public NGODetails ValidateNgo(int id,String password) {
		NGODetails ngodetails=dao.findById(id);
		if(!(ngodetails.getPassword().equals(password))){
			throw new InvalidUsernameAndPassword("Username And Password Not Matching");
		}
		else {
			return ngodetails;
		}
	}
	
	//for updating ngo password and flag
	public boolean UpdateNgoDetais(int id,int flag,String password) {
		return dao.UpdateNgoDetails(id, flag, password);
	}
	//for saving executive into DB
	public boolean AddExecutive(Executive executive) {
		return dao.AddExecutive(executive);
	}
	
	//for finding executive details by username
	public Executive findByExecutiveId(String username) {
		Executive exe=dao.findExecutiveById(username);
		if(exe==null) {
			throw new ResourceNotFoundException("No Executive with username:"+username);
		}
		return exe;
	}
	
	
	//for authenticating executive credientials
	public Executive AuthenticateExecutive(String username,String password) {
		Executive exe=dao.findExecutiveById(username);
		if(exe.getPassword().equals(password)) {
			return exe;
		}
		throw new InvalidUsernameAndPassword("Username or Password is InCorrect");
		
	}
}
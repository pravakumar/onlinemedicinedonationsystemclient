package com.omds.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.omds.dao.UserDao;
import com.omds.entities.User;
import com.omds.exceptions.InvalidUsernameAndPassword;
import com.omds.exceptions.ResourceNotFoundException;

@Service
public class UserServices {
	@Autowired
	private UserDao userdao;

	
	public boolean registerUser(User user) {
		return userdao.registerUser(user);
	}
	
	//for checking if userexists or not
	public User findUserByName(String username) {
		User user=userdao.findUserByName(username);
		if(user==null) {
			throw new ResourceNotFoundException("User does not Exists");
		}
		else {
			return user;
		}
	}
	
	//for checking username and password
	
	public User ValidateUser(String username,String password) {
		User user=userdao.findUserByName(username);
		if(!(user.getPassword().equals(password))) {
			throw new InvalidUsernameAndPassword("Username and Password Not Matching");
		}
		else {
			return user;
		}
		
	}
	

}

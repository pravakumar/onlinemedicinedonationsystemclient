package com.omds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMedicineDonationSystemServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineMedicineDonationSystemServerApplication.class, args);
	}

}

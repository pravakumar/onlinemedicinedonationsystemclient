package com.omds.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.omds.entities.AdminLogin;

public class AdminLoginResultSet implements ResultSetExtractor<AdminLogin>{

	@Override
	public AdminLogin extractData(ResultSet rs) throws SQLException, DataAccessException {
		AdminLogin admin=null;
		if(rs.next()) {
			admin=new AdminLogin();
			admin.setUsername(rs.getString(1));
			admin.setPassword(rs.getString(2));
		}
		return admin;
	}

}
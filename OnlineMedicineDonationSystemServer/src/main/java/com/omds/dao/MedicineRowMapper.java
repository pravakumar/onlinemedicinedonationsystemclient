package com.omds.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.omds.entities.Medicine;

public class MedicineRowMapper implements RowMapper<Medicine> {

	@Override
	public Medicine mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Medicine medicine =new Medicine();
		medicine.setMedicineid(rs.getInt(1));
		medicine.setMedicine_name(rs.getString(2));
		medicine.setMedicine_price(rs.getFloat(3));
		medicine.setQuantity(rs.getInt(4));
		medicine.setMfg_date(rs.getString(5));
		medicine.setExpire_date(rs.getString(6));
		return medicine;
	}
	

}

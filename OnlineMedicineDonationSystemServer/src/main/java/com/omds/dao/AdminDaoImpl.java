package com.omds.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.omds.entities.AdminLogin;
import com.omds.entities.Executive;
import com.omds.entities.Medicine;
import com.omds.entities.NGODetails;
import com.omds.entities.User;
@Component
public class AdminDaoImpl implements AdminDao{
	@Autowired
	private JdbcTemplate jdbctemplate;

	@Override
	public AdminLogin adminAuthenticate(String username) {
		// TODO Auto-generated method stub
		PreparedStatementSetter setter=new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
		
		return jdbctemplate.query("select * from admin where username=?", setter, new ResultSetExtractor<AdminLogin>() {

			@Override
			public AdminLogin extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				AdminLogin adm=null;
				if(rs.next()) {
					adm=new AdminLogin();
					adm.setUsername(rs.getString(1));
					adm.setPassword(rs.getString(2));
					return adm;
					
				}
				return adm;
			}
		});
	}

	@Override
	public boolean AddNgo(NGODetails ngodetails) {
		// TODO Auto-generated method stub
		int res=jdbctemplate.update("insert into NGODetails values(?,?,?,?,?,?,?,?)",ngodetails.getId(),ngodetails.getName(),
				ngodetails.getEmailid(),ngodetails.getPassword(),ngodetails.getContact_num(),ngodetails.getCity(),ngodetails.getState()
				,ngodetails.getFlag());
		if(res>=1) {
			return true;
			
		}
		return false;
	}

	@Override
	public List<NGODetails> ViewNgo() {
		// TODO Auto-generated method stub
		
		return jdbctemplate.query("select * from NGODetails", new NGODetailsRowMapper());
	}

	@Override
	public List<User> ViewUsers() {
		// TODO Auto-generated method stub
		return jdbctemplate.query("select * from User", new UserRowMapper());
	}

	@Override
	public boolean AddMedicine(Medicine medicine) {
		// TODO Auto-generated method stub
		int res=jdbctemplate.update("insert into medicine(medicine_name,medicine_price,quantity,mfg_date,expire_date) values(?,?,?,?,?)",
				medicine.getMedicine_name(),medicine.getMedicine_price(),medicine.getQuantity(),medicine.getMfg_date(),medicine.getExpire_date());
		if(res>=1) {
			return true;
			
		}return false;
	}

	@Override
	public List<Medicine> ViewMedicine() {
		// TODO Auto-generated method stub
		
		return jdbctemplate.query("select * from medicine", new MedicineRowMapper());
	}

	@Override
	public NGODetails findById(int id) {
PreparedStatementSetter setter=new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setInt(1, id);
				
			}
		};
		
		return jdbctemplate.query("select * from  ngodetails where id=?", setter, new ResultSetExtractor<NGODetails>() {

			@Override
			public NGODetails extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				NGODetails ngodetails=null;
				if(rs.next()) {
					ngodetails=new NGODetails();
					ngodetails.setId(rs.getInt(1));
					ngodetails.setName(rs.getString(2));
					ngodetails.setEmailid(rs.getString(3));
					ngodetails.setPassword(rs.getString(4));
					ngodetails.setContact_num(rs.getString(5));
					ngodetails.setCity(rs.getString(6));
					ngodetails.setState(rs.getString(7));
					ngodetails.setFlag(rs.getInt(8));
					return ngodetails;
				}
				return ngodetails;
			}
		});
	}

	@Override
	public boolean UpdateNgoDetails(int id, int flag, String password) {
		// TODO Auto-generated method stub
		int res=jdbctemplate.update("update ngodetails set password=?,flag=? where id=?",password,flag,id);
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean AddExecutive(Executive executive) {
		int res=jdbctemplate.update("insert into executive(username,password,emailid,address,contact_num) values(?,?,?,?,?)",executive.getUsername(),
				executive.getPassword(),executive.getEmailid(),executive.getAddress(),executive.getContact_num());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public Executive findExecutiveById(String username) {
		
PreparedStatementSetter setter=new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
		
		return jdbctemplate.query("select * from executive where username=?", setter, new ResultSetExtractor<Executive>() {

			@Override
			public Executive extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				Executive exe=null;
				if(rs.next()) {
					exe=new Executive();
					exe.setExecutiveid(rs.getInt(1));
					exe.setUsername(rs.getString(2));
					exe.setPassword(rs.getString(3));
					exe.setEmailid(rs.getString(4));
					exe.setAddress(rs.getString(5));
					exe.setContact_num(rs.getString(6));
					return exe;
					
				}
				return exe;
			}
		});
	}

}



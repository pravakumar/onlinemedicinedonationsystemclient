package com.omds.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.omds.entities.AdminLogin;
import com.omds.entities.Executive;
import com.omds.entities.Medicine;
import com.omds.entities.NGODetails;
import com.omds.entities.User;

@Component
public interface AdminDao {
	public AdminLogin adminAuthenticate(String username);//for validating admin credientials
	public boolean AddNgo(NGODetails ngodetails);//for adding NGO into DB
	public List<NGODetails> ViewNgo();//for viewing all NGO from DB
	public List<User> ViewUsers();//for viewing all Users from DB
	public boolean AddMedicine(Medicine medicine);//for adding medicine to  DB
	public List<Medicine> ViewMedicine();//for viewing all the medicine
	public NGODetails findById(int id);//finding the ngodetails by id
	public boolean UpdateNgoDetails(int id,int flag,String password);//updating password and flag by id for ngodetails
	public boolean AddExecutive(Executive executive);//for adding executive into DB
	public Executive findExecutiveById(String username);//for authentication and validation of execuitve
	

}

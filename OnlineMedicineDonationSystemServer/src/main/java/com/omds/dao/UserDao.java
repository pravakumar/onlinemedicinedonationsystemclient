package com.omds.dao;

import javax.jws.soap.SOAPBinding.Use;

import org.springframework.stereotype.Component;

import com.omds.entities.User;

@Component
public interface UserDao {
	
	public boolean registerUser(User user);//for registrating user
	public User findUserByName(String username);//for checking if user exists or not and validating username and password
//	public boolean ValidateUserNameAndPassword()

}

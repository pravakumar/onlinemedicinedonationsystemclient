package com.omds.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;

import com.omds.entities.NGODetails;

public class NGODetailsRowMapper implements org.springframework.jdbc.core.RowMapper<NGODetails> {

	@Override
	public NGODetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		NGODetails ngodetails=new NGODetails();
		ngodetails.setId(rs.getInt(1));
		ngodetails.setName(rs.getString(2));
		ngodetails.setEmailid(rs.getString(3));
		ngodetails.setPassword(rs.getString(4));
		ngodetails.setContact_num(rs.getString(5));
		ngodetails.setCity(rs.getString(6));
		ngodetails.setState(rs.getString(7));
		
		return ngodetails;
	}

	
}

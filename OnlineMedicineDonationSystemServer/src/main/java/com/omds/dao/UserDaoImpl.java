package com.omds.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.omds.entities.User;

@Component
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private JdbcTemplate jdbctemplate;

	@Override
	public boolean registerUser(User user) {
		// TODO Auto-generated method stub
		int res=jdbctemplate.update("insert into user(username,password,emailid,address,contact_num) values(?,?,?,?,?)",user.getUsername(),user.getPassword(),user.getEmailid(),
				user.getAddress(),user.getContact_num());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public User findUserByName(String username) {
		// TODO Auto-generated method stub
		PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
		return jdbctemplate.query("select * from user where username=?", setter, new ResultSetExtractor<User>() {

			@Override
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {
				// TODO Auto-generated method stub
				User user=null;
				if(rs.next()) {
					user=new User();
					user.setUserid(rs.getInt(1));
					user.setUsername(rs.getString(2));
					user.setPassword(rs.getString(3));
					user.setEmailid(rs.getString(4));
					user.setAddress(rs.getString(5));
					user.setContact_num(rs.getString(6));
				}
				return user;
			}
		});
	}
	
	

}
